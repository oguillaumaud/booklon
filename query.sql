-- Active: 1709546324780@@127.0.0.1@3306@booklon
--Afficher le planing des centres
SELECT center.name, booking.`startDate`, booking.`endDate`, promo.name, room.name FROM promo
LEFT JOIN booking ON promo.id = booking.id_room
LEFT JOIN room ON room.id = booking.id_promo
LEFT JOIN center ON promo.id_center = center.id;
--Affiché le planning par promo
SELECT promo.name, booking.`startDate`, booking.`endDate` FROM promo
LEFT JOIN booking ON promo.id = booking.id_room
LEFT JOIN room ON room.id = booking.id_promo;
--Changer les prériode des promos
UPDATE promo SET `startDate`="2024-02-18"  WHERE id = 1;
UPDATE promo SET `endDate`="2024-08-30" WHERE id = 1;

--Afficher planning par salle
SELECT room.name, booking.`startDate`, booking.`endDate` FROM room
LEFT JOIN booking ON room.id = booking.id_room
LEFT JOIN promo ON promo.id = booking.id_promo;

--Afficher plannig du jour
SELECT NOW(), room.name, promo.name FROM room
LEFT JOIN booking ON room.id = booking.id_room
LEFT JOIN promo ON promo.id = booking.id_promo;


--Liste des centre pour chaque région
SELECT region.name, center.name FROM center
LEFT JOIN region ON region.id = center.id_region;


--Gestion des promos
--liste les promo d'un centre et ces student
SELECT  promo.name, student.name, promo.`startDate`, promo.`endDate`, promo.studentNumber FROM promo
LEFT JOIN student_promo ON student_promo.id_promo=promo.id
LEFT JOIN student ON student_promo.id_student=student.id;

--liste des promo d'un centre
SELECT center.name, promo.name FROM center LEFT JOIN promo ON promo.id_center = center.id;

-- modification de la liste de student
-- Ajouté un student
INSERT INTO student(name) VALUES ("Choubaka");
--Attribué une promo a un student
INSERT INTO student_promo(id_promo, id_student) VALUES (1,7);
--Supprimer un student
DELETE FROM student WHERE id=7;
SELECT * From student;


--liste des des salle d'un centre
SELECT room.*, center.name FROM room LEFT JOIN center ON  room.id_center = center.id;

-- Gérer la capacity d'un room
UPDATE room SET capacity = 10 WHERE name="CSS";
SELECT capacity FROM room;


-- Modiffier une réservation
UPDATE booking SET `startDate`="2024-05-18"  WHERE id = 1;
UPDATE booking SET `endDate`="2024-05-30" WHERE id = 1;
UPDATE booking SET id_room=3 AND id_promo =1 WHERE id = 1;

-- Supprimer une réservation
DELETE FROM booking WHERE id = 1;


--Selectionné son centre par défaut
SELECT name FROM center WHERE center.name = "Lyon" OR center.name = "Saint Etienne" OR center.name = "Grenoble";
