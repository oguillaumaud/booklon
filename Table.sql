-- Active: 1709546324780@@127.0.0.1@3306@booklon
DROP TABLE IF EXISTS student_promo;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS region;

CREATE TABLE region(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE center(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    id_region INT,
    Foreign Key (id_region) REFERENCES region(id) ON DELETE SET NULL
);

CREATE TABLE room(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100)NOT NULL,
    capacity INT NOT NULL,
    color VARCHAR(50),
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id) ON DELETE SET NULL
);


CREATE TABLE promo(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    startDate DATE NOT NULL,
    endDate DATE NOT NULL,
    studentNumber INT,
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id) ON DELETE SET NULL
);

CREATE TABLE booking(
    id INT PRIMARY KEY AUTO_INCREMENT,
    startDate DATE NOT NULL,
    endDate DATE NOT NULL,
    id_room INT,
    Foreign Key (id_room) REFERENCES room(id) ON DELETE SET NULL,
    id_promo INT,
    Foreign Key (id_promo) REFERENCES promo(id) ON DELETE SET NULL
);

CREATE TABLE student(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE student_promo(
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_student INT ,
    Foreign KEY (id_student) REFERENCES student(id) ON DELETE SET NULL,
    id_promo INT,
    Foreign KEY (id_promo) REFERENCES promo(id) ON DELETE SET NULL
);

INSERT INTO region (name) VALUES
("Loire"), 
("Rhône"), 
("Isère");

INSERT INTO center(name,id_region) VALUES
("Lyon", 2), 
("Grenoble",3), 
("Saint Etienne",1);

INSERT INTO room(name, capacity, color, id_center) VALUES
("CSS", 25, "Blue", 1 AND 2), 
("Python", 40, "Red", 3 AND 2), 
("Javascript", 18, "Green",1 AND 2 AND 3);

INSERT INTO promo (name, `startDate`,`endDate`, studentNumber, id_center) VALUES
("DevWeb","2023-11-08","2024-12-20", 15, 3),
("CDA","2024-02-15","2025-10-30", 16, 1),
("Cloud","2024-01-08","2024-12-06", 18, 2);

INSERT INTO booking(`startDate`,`endDate`, id_room, id_promo) VALUES
("2023-11-08","2024-12-20", 1,2 ),
("2024-02-15","2025-10-30", 3,3),
("2024-01-08","2024-12-06", 2,1);

INSERT INTO student(name) VALUES
("Bobby"), 
("Billy"), 
("Bob"), 
("Albert"), 
("Marie"), 
("Josianne");

INSERT INTO student_promo(id_promo, id_student) VALUES
(3,1),
(1,2),
(2,3),
(2,4),
(1,5),
(3,6);

